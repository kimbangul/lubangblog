package com.kr.common.app;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.Configuration;

@Configuration
public class SqlSessionConfig {        

    String resource = "config/mybatis-config.xml";		
    SqlSessionFactory sqlSessionFactory;
    
    @Bean
    public SqlSessionFactory sessionFactory() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream(resource);		
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);		
        return sqlSessionFactory;
    }
}
