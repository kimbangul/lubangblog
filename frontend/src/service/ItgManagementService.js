import http from "../http-common";

class ItgManagementService {
    createIntegratedMaster(data) {
        return http.post("/api/createIntegratedMaster", data);
    }
    deleteIntegratedMaster(data) {
        return http.post("/api/deleteIntegratedMaster", data);
    }
    selectIntegratedMaster(values) {
        return http.get("/api/selectIntegratedMaster", {
            params: {
                txtCodeValue: values,
            },
        });
    }

    createIntegratedDetail(data) {
        return http.post("/api/createIntegratedDetail", data);
    }
    deleteIntegratedDetail(data) {
        return http.post("/api/deleteIntegratedDetail", data);
    }
    selectIntegratedDetail(values) {
        return http.get("/api/selectIntegratedDetail", {
            params: {
                txtCodeValue: values.txtCodeValue,
                txtCodeDetailValue: values.txtCodeDetailValue,
            },
        });
    }

    getMaseterID() {
        return http.get("/api/getMaseterID");
    }
}

export default new ItgManagementService();
