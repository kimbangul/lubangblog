import http from "../http-common";

class SkillDataService {
    get() {
        return http.get("/api/skill");
    }
}

export default new SkillDataService();
