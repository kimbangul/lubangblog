import http from "../http-common";

class DataMigrationService {
    CreateExcel() {
        return http({
            method: "GET",
            url: "/dataMigration/create_excel",
            responseType: "blob",
        });
    }
    SelectExcelInf() {
        return http({
            method: "GET",
            url: "/dataMigration/excel_inf",
        });
    }
}

export default new DataMigrationService();
