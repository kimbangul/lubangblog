import http from "../http-common";

class commonService {
    GetDateOfBirthList(date_type) {
        return http.get("/common/getDateOfBirthList", {
            params: {
                date_type: date_type,
            },
        });
    }
    GetIntegratedCode(codeype_id) {
        return http.get("/common/getIntegratedCode", {
            params: {
                codeype_id: codeype_id,
            },
        });
    }
}

export default new commonService();
