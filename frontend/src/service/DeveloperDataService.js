import http from "../http-common";

class DeveloperDataService {
    getAll() {
        return http.get("/api/developers");
    }

    get(id) {
        return http.get(`/api/developers/${id}`);
    }

    create(data) {
        return http.post("/api/developers", data);
    }

    update(id, data) {
        return http.put(`/api/developers/${id}`, data);
    }

    delete(id) {
        return http.delete(`/api/developers/${id}`);
    }

    deleteAll() {
        return http.delete(`/api/developers`);
    }

    findByName(name) {
        return http.get(`/api/developers?name=${name}`);
    }

    search(condition) {
        const params = condition;
        console.log(params);
        return http.get("/api/developers/search", {
            params: {
                skills: condition.skills,
                grade: condition.grade,
            },
        });
    }
}

export default new DeveloperDataService();
