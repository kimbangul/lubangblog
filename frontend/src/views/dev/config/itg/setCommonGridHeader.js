export default {
    setColoumnDefault: (
        columnDefs,
        headerName,
        field,
        resizable,
        sortable,
        width,
        editable,
        cellEditorUse,
        cellEditorType,
        selectListValues,
        checkboxSelection,
    ) => {
        if (cellEditorUse) {
            var Headers = {
                headerName: headerName,
                field: field,
                resizable: resizable,
                sortable: sortable,
                width: width,
                editable: editable,
                cellEditor: cellEditorType,
                cellEditorParams: {
                    values: selectListValues,
                },
                checkboxSelection: checkboxSelection,
            };
        } else {
            Headers = {
                headerName: headerName,
                field: field,
                resizable: resizable,
                sortable: sortable,
                editable: editable,
                width: width,
                checkboxSelection: checkboxSelection,
            };
        }
        columnDefs.push(Headers);
        return columnDefs;
    },
};
