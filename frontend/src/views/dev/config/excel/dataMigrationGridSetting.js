export default [
    {
        headerName: "제외경력\n(개월수)", //
        field: "career_month",
        resizable: true,
        sortable: true,
        width: 150,
    },
    {
        headerName: "년수", //
        field: "career_month",
        resizable: true,
        sortable: true,
        width: 100,
    },
    {
        headerName: "등급", //
        field: "grade",
        resizable: true,
        sortable: true,
        width: 100,
    },
    // {
    //     headerName: "기술구분", //
    //     field: "city",
    //     resizable: true,
    //     sortable: true,
    // },
    // {
    //     headerName: "기술", //
    //     field: "date_available",
    //     resizable: true,
    //     sortable: true,
    // },
    // {
    //     headerName: "대표기술", //
    //     field: "date_last_update",
    //     resizable: true,
    //     sortable: true,
    // },
    // {
    //     headerName: "상세기술", //
    //     field: "description",
    //     resizable: true,
    //     sortable: true,
    // },
    // {
    //     headerName: "대표업종", //
    //     field: "dm_allowed",
    //     resizable: true,
    //     sortable: true,
    // },
    // {
    //     headerName: "고객사(PJT)(삼성그룹외)", //
    //     field: "email",
    //     resizable: true,
    //     sortable: true,
    // },
    // {
    //     headerName: "SPA자격증(취득년월)",
    //     field: "freelancer",
    //     resizable: true,
    //     sortable: true,
    // },
    {
        headerName: "졸업학교", //
        field: "school",
        resizable: true,
        sortable: true,
        width: 150,
    },
    {
        headerName: "졸업년도", //
        field: "school_final",
        resizable: true,
        sortable: true,
        width: 150,
    },
    {
        headerName: "비고", //
        field: "description",
        resizable: true,
        sortable: true,
    },
    // {
    //     headerName: "등급Key", //
    //     field: "jungchegi",
    //     resizable: true,
    //     sortable: true,
    // },
];
