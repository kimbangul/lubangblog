import { createWebHistory, createRouter } from "vue-router";
// import registration from "@/views/dev/registration.vue";
// import detail from "@/views/dev/detail.vue";
// import list from "@/views/dev/list.vue";
// import skill from "@/views/dev/skill.vue";
// import search from "@/views/dev/search.vue";
// import home from "@/views/dev/home.vue";
// import dataMigration from "@/views/dev/dataMigration.vue";
// import ItgManagement from "@/views/dev/ItgManagement.vue";
import imageSlider from "../components/ImageSlider.vue";
import joinMembershipRegister from "@/views/dev/joinMembershipRegister.vue";
import Test2 from "../../src/Test2.vue";
const routes = [
    {
        path: "/",
        component: imageSlider,
    },
    // {
    //     path: "/registration",
    //     component: registration,
    // },
    // {
    //     path: "/detail/:id",
    //     component: detail,
    // },
    // {
    //     path: "/list",
    //     name: "lists",
    //     component: list,
    // },
    // {
    //     path: "/skill",
    //     component: skill,
    // },
    // {
    //     path: "/search",
    //     component: search,
    // },
    // {
    //     path: "/dataMigration",
    //     component: dataMigration,
    // },
    // {
    //     path: "/itgManagement",
    //     component: ItgManagement,
    // },
    // {
    //     path: "/login",
    //     component: login,
    // },
    {
        path: "/joinMembershipRegister",
        component: joinMembershipRegister,
    },
    {
        path: "/Login",
        component: Test2,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
