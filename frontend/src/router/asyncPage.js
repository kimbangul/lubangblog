export default function asyncPage(component) {
    return () => ({
        component,
        delay: 150,
        timeout: 5000,
    });
}
