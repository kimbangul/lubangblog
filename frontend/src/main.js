import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import CommonGrid from "./components/CommonGrid.vue";

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";

const app = createApp(App);
app.component("common-grid", CommonGrid);

app.use(router).mount("#app");
